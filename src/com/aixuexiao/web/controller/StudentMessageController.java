package com.aixuexiao.web.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.aixuexiao.model.Classes;
import com.aixuexiao.model.Grid;
import com.aixuexiao.model.StudentMessage;
import com.aixuexiao.service.StudentMessageService;
import com.aixuexiao.service.StudentService;

/**
 * 包含员工列表菜单内的所有操作
 */
@Controller()
public class StudentMessageController {
	public static final int pagesize = 8;
	@Resource(name="studentService")
	private StudentService studentService;
	@Resource(name="studentMessageService")
	private StudentMessageService studentMessageService;
	
	@RequestMapping(value="/manager/studentmessages")
	public ModelAndView listStudent(String pagenum,HttpServletRequest request){
		ModelAndView mv=new ModelAndView();
		StudentMessage studentMessage = new StudentMessage();
		studentMessage.setSname(request.getParameter("sname"));
		String classid = request.getParameter("classid");
		if(!StringUtils.isEmpty(classid))
			studentMessage.setClassid(Integer.parseInt(classid));
		String studentid = request.getParameter("studentid");
		if(!StringUtils.isEmpty(studentid))
			studentMessage.setStudentid(Integer.parseInt(studentid));
		studentMessage.setContent(request.getParameter("content"));
		
		mv.setViewName("studentmessages");
		mv.addObject("sidebar","studentmessages");
		int num = 1;
		if(null!=pagenum && !StringUtils.isEmpty(pagenum)){
			num = Integer.parseInt(pagenum);
		}
		List<StudentMessage> list = studentMessageService.listStudentMessage((num-1)*pagesize, pagesize,studentMessage);
		int count = studentMessageService.countStudentMessage(studentMessage);
		Grid<StudentMessage> grid = new Grid<StudentMessage>();
		grid.setRows(list);
		grid.setTotal(count);
		grid.setCurrentPage(num);
		grid.setPageNum(pagesize);
		mv.addObject("grid",grid);
		List<Classes> clslist = studentService.findAllClasses();
		mv.addObject("clsList", clslist);
		mv.addObject("studentMessage", studentMessage);
		return mv;
	}
	
	@RequestMapping(value="/manager/updatestudentmessage",method=RequestMethod.POST)
	public ModelAndView updateStudentMessage(StudentMessage studentMessage){
		ModelAndView mv=new ModelAndView();
		studentMessageService.updateStudentMessage(studentMessage); 
		mv.addObject("notice","回复信息成功");
		mv.setViewName("redirect:/manager/studentmessages");
		return mv;
	}
	
	@RequestMapping(value="/manager/deletestudentmessage",method=RequestMethod.GET)
	public ModelAndView deleteStudentMessage(int id){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("redirect:/manager/studentmessages");
		studentMessageService.deleteStudentMessage(id);
		mv.addObject("notice","删除反馈信息成功");
		return mv;
	}
}
