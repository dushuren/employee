package com.aixuexiao.web.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.aixuexiao.model.Grid;
import com.aixuexiao.model.Score;
import com.aixuexiao.service.ScoreService;

/**
 * 用户积分的所有操作
 */
@Controller()
public class ScoreController {
	public static final int pagesize = 8;
	
	@Resource(name="scoreService")
	private ScoreService scoreService;
	
	
	@RequestMapping(value="/manager/scores")
	public ModelAndView listScore(String pagenum,HttpServletRequest request){
		Score score = new Score();
		String id = request.getParameter("id");
		if(!StringUtils.isEmpty(id))
			score.setId(Integer.parseInt(id));
		ModelAndView mv=new ModelAndView();
		mv.setViewName("scores");
		mv.addObject("sidebar","scores");
		int num = 1;
		if(null!=pagenum && !StringUtils.isEmpty(pagenum)){
			num = Integer.parseInt(pagenum);
		}
		List<Score> list = scoreService.listScore((num-1)*pagesize, pagesize,score);
		int count = scoreService.countScore(score);
		Grid<Score> grid = new Grid<Score>();
		grid.setRows(list);
		grid.setTotal(count);
		grid.setCurrentPage(num);
		grid.setPageNum(pagesize);
		mv.addObject("grid",grid);
		mv.addObject("length", list.size());
		mv.addObject("score", score);
		if(score.getId()!=0)
		mv.addObject("scoreId",score.getId());
		return mv;
	}
}
