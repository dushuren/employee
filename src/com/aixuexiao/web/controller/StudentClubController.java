package com.aixuexiao.web.controller;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.aixuexiao.model.Grid;
import com.aixuexiao.model.StudentClub;
import com.aixuexiao.service.ClubService;
import com.aixuexiao.service.StudentClubService;

/**
 * 包含员工列表菜单内的所有操作
 */
@Controller()
public class StudentClubController {
	public static final int pagesize = 8;
	@Resource(name="clubService")
	private ClubService clubService;
	@Resource(name="studentClubService")
	private StudentClubService studentClubService;
	
	
	@RequestMapping(value="/manager/studentclubmanagerpage",method=RequestMethod.GET)
	public ModelAndView studentPage(String pagenum,StudentClub student,String studentId){
		ModelAndView mv=new ModelAndView();
		int num = 1;
		if(null!=pagenum){
			num = Integer.parseInt(pagenum);
		}
		if(!StringUtils.isEmpty(studentId))
			student.setStudentid(Integer.parseInt(studentId));
		List<StudentClub> stlist = studentClubService.listStudent((num-1)*pagesize, pagesize,student);
		int count = studentClubService.count(student);
		Grid<StudentClub> grid = new Grid<StudentClub>();
		grid.setRows(stlist);
		grid.setTotal(count);
		grid.setCurrentPage(num);
		grid.setPageNum(pagesize);
		
		mv.setViewName("addstudentclub");
		mv.addObject("clubid",student.getClubid());
		mv.addObject("club",clubService.findClubById(student.getClubid()));
		mv.addObject("sidebar","club");
		mv.addObject("studentClub",student);
		mv.addObject("type",student.getStatus());
		mv.addObject("grid",grid);
		mv.addObject("studentId",studentId);
		return mv;
	}
	
	@RequestMapping(value="/manager/deletestudentclub",method=RequestMethod.GET)
	public ModelAndView deletemessage(int id,int clubid){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("redirect:/manager/studentclubmanagerpage");
		studentClubService.deleteStudentClubById(id);
		mv.addObject("club",clubService.findClubById(clubid));
		mv.addObject("notice","删除成员申请成功");
		return mv;
	}
	
	@RequestMapping(value="/manager/studentclubchangestate",method=RequestMethod.GET)
	public ModelAndView clubchangestate(int id,int status,int clubid){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("redirect:/manager/studentclubmanagerpage");
		studentClubService.studentclubchangestate(id,status);
		mv.addObject("club",clubService.findClubById(clubid));
		mv.addObject("notice","成员申请通过");
		return mv;
	}
}
