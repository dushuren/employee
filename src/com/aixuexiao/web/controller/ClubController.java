package com.aixuexiao.web.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.aixuexiao.model.Club;
import com.aixuexiao.model.Grid;
import com.aixuexiao.service.ClubService;
import com.aixuexiao.service.StudentClubService;
import com.aixuexiao.service.StudentService;

@Controller
public class ClubController {
	
	
	public static int pagesize = 10;
	
	@Resource(name="clubService")
	private ClubService clubService;
	@Resource(name="studentService")
	private StudentService studentService;
	@Resource(name="studentClubService")
	private StudentClubService studentClubService;
	
	@RequestMapping(value="/manager/club")
	public ModelAndView listStudent(String pagenum,HttpServletRequest request){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("club");
		mv.addObject("sidebar","club");
		int num = 1;
		if(null!=pagenum && !StringUtils.isEmpty(pagenum)){
			num = Integer.parseInt(pagenum);
		}
		Club club = new Club();
		club.setName(request.getParameter("name"));
		List<Club> list = clubService.listClub((num-1)*pagesize, pagesize,club);
		int count = clubService.countClub(club);
		Grid<Club> grid = new Grid<Club>();
		grid.setRows(list);
		grid.setTotal(count);
		grid.setCurrentPage(num);
		grid.setPageNum(pagesize);
		mv.addObject("grid",grid);
		mv.addObject("club", club);
		return mv;
	}

	
	@RequestMapping(value="/manager/addclubpage",method=RequestMethod.GET)
	public ModelAndView addClubPage(){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("addclub");
		mv.addObject("sidebar","club");
		return mv;
	}
	
	@RequestMapping(value="/manager/addclub",method=RequestMethod.POST)
	public ModelAndView addClub(Club club){
		ModelAndView mv=new ModelAndView();
		Club cls = clubService.findClubById(club.getId());
		if(null==cls){
			mv.setViewName("redirect:/manager/club");
			clubService.addClub(club);
		}else{
			mv.setViewName("redirect:/manager/addclubpage");
			mv.addObject("name", club.getName());
			mv.addObject("notice","已存在编号为"+club.getId()+"的俱乐部");
		}
		return mv;
	}
	@RequestMapping(value="/manager/updateclub",method=RequestMethod.POST)
	public ModelAndView updateclub(Club club){
		ModelAndView mv=new ModelAndView();
		clubService.updateClub(club); 
		mv.addObject("notice","编辑俱乐部信息成功");
		mv.setViewName("redirect:/manager/club");
		return mv;
	}
	
	/**   
	 * @Title: clubchangestate   
	 * @Description: 修改俱乐部状态 
	 * @param id
	 * @return
	 * @author  author
	 */
	 
	@RequestMapping(value="/manager/clubchangestate",method=RequestMethod.GET)
	public ModelAndView clubchangestate(int id,int status){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("redirect:/manager/club");
		clubService.clubchangestate(id,status);
		mv.addObject("notice","俱乐部状态修改成功");
		return mv;
	}
	
	
}
