package com.aixuexiao.web.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.aixuexiao.model.Classes;
import com.aixuexiao.model.Grid;
import com.aixuexiao.model.PrizeNote;
import com.aixuexiao.service.PrizeNoteService;
import com.aixuexiao.service.StudentService;

/**
 * 奖品领取记录的所有操作
 */
@Controller
public class PrizeNoteController {
	public static final int pagesize = 8;
	@Resource(name="studentService")
    private StudentService studentService;
	@Resource(name="prizeNoteService")
	private PrizeNoteService prizeNoteService;
	
	
	@RequestMapping(value="/manager/prizeNotes")
	public ModelAndView listPrizeNote(String pagenum,HttpServletRequest request){
	    PrizeNote prizeNote = new PrizeNote();
        prizeNote.setSname(request.getParameter("sname"));
        String classid = request.getParameter("classid");
        if(!StringUtils.isEmpty(classid))
            prizeNote.setClassid(Integer.parseInt(classid));
        String userid = request.getParameter("userid");
        if(!StringUtils.isEmpty(userid))
            prizeNote.setUserid(Integer.parseInt(userid));
        
        ModelAndView mv=new ModelAndView();
        mv.setViewName("prizeNotes");
        mv.addObject("sidebar","prizeNotes");
        int num = 1;
        if(null!=pagenum && !StringUtils.isEmpty(pagenum)){
            num = Integer.parseInt(pagenum);
        }
        List<PrizeNote> list = prizeNoteService.listPrizeNote((num-1)*pagesize, pagesize,prizeNote);
        int count = prizeNoteService.countPrizeNote(prizeNote);
        Grid<PrizeNote> grid = new Grid<PrizeNote>();
        grid.setRows(list);
        grid.setTotal(count);
        grid.setCurrentPage(num);
        grid.setPageNum(pagesize);
        mv.addObject("grid",grid);
        mv.addObject("prizeNote", prizeNote);
        List<Classes> clslist = studentService.findAllClasses();
        mv.addObject("clsList", clslist);
        return mv;
	}
}
