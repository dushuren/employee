package com.aixuexiao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 奖品兑换记录实体
 */
public class PrizeNote implements Serializable {
	
	
	 /**
	 * 
	 */
	 
	private static final long serialVersionUID = 1L;

	private int id;
	/**
	 * 用户id
	 */
	private int userid;
	/**
	 * 签到日期
	 */
	private Date day;
	/**
	 * 总积分
	 */
	private int score;
	/**
	 * 奖品id
	 */
	private int prizeid;
	/**
	 * 奖品活动id
	 */
	private int prizeactiveid;
	private String sname;
    private String cname;
    private int classid;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public int getPrizeid() {
		return prizeid;
	}

	public void setPrizeid(int prizeid) {
		this.prizeid = prizeid;
	}

	public int getPrizeactiveid() {
		return prizeactiveid;
	}

	public void setPrizeactiveid(int prizeactiveid) {
		this.prizeactiveid = prizeactiveid;
	}

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

}
