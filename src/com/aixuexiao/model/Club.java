/*
 * 文件名：Club.java
 * 版权：Copyright by 791440142
 * 描述：
 * 修改人：yu_qhai
 * 修改时间：2016年3月21日
 */

package com.aixuexiao.model;

import java.io.Serializable;

public class Club implements Serializable{
	
	 /**
	 * 
	 */
	 
	private static final long serialVersionUID = 1L;

	/**
	 * 班级编号 
	 */
	private int id;
	
	/**
	 * 班级名称
	 */
	private String name;
	
	 /**
	 * 描述
	 */
	 
	private String des;
	private int status;
	
	 /**
	 * 详细页面
	 */
	 
	private String url;
	
	
	 /**
	 * 待审核成员
	 */
	 
	private int outstatus;
	
	 /**
	 * 已审核成员
	 */
	 
	private int instatus;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getOutstatus() {
		return outstatus;
	}

	public void setOutstatus(int outstatus) {
		this.outstatus = outstatus;
	}

	public int getInstatus() {
		return instatus;
	}

	public void setInstatus(int instatus) {
		this.instatus = instatus;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}
