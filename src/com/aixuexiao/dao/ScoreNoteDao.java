package com.aixuexiao.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.aixuexiao.model.ScoreNote;
import com.aixuexiao.util.DateUtils;

@Component("scoreNoteDao")
public class ScoreNoteDao extends BaseDao {


	public ScoreNote findScoreNoteById(int id) {
		return this.readSqlSession.selectOne("com.aixuexiao.dao.ScoreNoteDao.selectScoreNoteById",id);
	}
	public List<ScoreNote> findScoreNote(int start,int size,ScoreNote scoreNote) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("scoreNote", scoreNote);
		return this.readSqlSession.selectList("com.aixuexiao.dao.ScoreNoteDao.selectScoreNote",map);
	}
	public int countScoreNote(ScoreNote scoreNote) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("scoreNote", scoreNote);
		return this.readSqlSession.selectOne("com.aixuexiao.dao.ScoreNoteDao.selectScoreNoteCount",map);
	}
	
	public int addScoreNote(ScoreNote scoreNote) {
		return this.writerSqlSession.insert("com.aixuexiao.dao.ScoreNoteDao.addScoreNote", scoreNote);
	}
	
	public int deleteScoreNoteById(int scoreNoteid) {
		return this.writerSqlSession.delete("com.aixuexiao.dao.ScoreNoteDao.deleteScoreNoteById", scoreNoteid);
	}
	public List<Map<String,Object>> getMonthScoreNoteById(int userid,Date date){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("userid", userid);
		date = date ==null?new Date():date;
		String month = DateUtils.format(date, "yyyy-MM"); 
		map.put("month", month);
		return this.readSqlSession.selectList("com.aixuexiao.dao.ScoreNoteDao.selectMonthScoreNoteById",map);
	}
	public boolean isTodayScore(int userid){
		int count = this.readSqlSession.selectOne("com.aixuexiao.dao.ScoreNoteDao.isTodayScore",userid);
		return count>0;
	}
}
