package com.aixuexiao.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.aixuexiao.model.PrizeNote;

@Component("prizeNoteDao")
public class PrizeNoteDao extends BaseDao {


	public PrizeNote findPrizeNoteById(int id) {
		return this.readSqlSession.selectOne("com.aixuexiao.dao.PrizeNoteDao.selectPrizeNoteById",id);
	}
	public List<PrizeNote> findPrizeNote(int start,int size,PrizeNote prizeNote) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("prizeNote", prizeNote);
		return this.readSqlSession.selectList("com.aixuexiao.dao.PrizeNoteDao.selectPrizeNote",map);
	}
	public int countPrizeNote(PrizeNote prizeNote) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("prizeNote", prizeNote);
		return this.readSqlSession.selectOne("com.aixuexiao.dao.PrizeNoteDao.selectPrizeNoteCount",map);
	}
	
	public int addPrizeNote(PrizeNote prizeNote) {
		return this.writerSqlSession.insert("com.aixuexiao.dao.PrizeNoteDao.addPrizeNote", prizeNote);
	}
	
	public int deletePrizeNoteById(int prizeNoteid) {
		return this.writerSqlSession.delete("com.aixuexiao.dao.PrizeNoteDao.deletePrizeNoteById", prizeNoteid);
	}
	public int isPrizeNoteByUserid(int userid){
		return this.readSqlSession.selectOne("com.aixuexiao.dao.PrizeNoteDao.selectPrizeNoteByUserid",userid);
	}
}