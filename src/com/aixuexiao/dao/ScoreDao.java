package com.aixuexiao.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.aixuexiao.model.Score;

@Component("scoreDao")
public class ScoreDao extends BaseDao {


	public Score findScoreById(int id) {
		return this.readSqlSession.selectOne("com.aixuexiao.dao.ScoreDao.selectScoreById",id);
	}
	public Score findScoreByUserId(int userid) {
		return this.readSqlSession.selectOne("com.aixuexiao.dao.ScoreDao.selectScoreByUserId",userid);
	}
	public List<Score> findScore(int start,int size,Score score) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("score", score);
		return this.readSqlSession.selectList("com.aixuexiao.dao.ScoreDao.selectScore",map);
	}
	public int countScore(Score score) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("score", score);
		return this.readSqlSession.selectOne("com.aixuexiao.dao.ScoreDao.selectScoreCount",map);
	}
	
	public int addScore(Score score) {
		return this.writerSqlSession.insert("com.aixuexiao.dao.ScoreDao.addScore", score);
	}
	
	public int deleteScoreById(int scoreid) {
		return this.writerSqlSession.delete("com.aixuexiao.dao.ScoreDao.deleteScoreById", scoreid);
	}
	
	public int updateScore(Score score) {
		return this.writerSqlSession.update("com.aixuexiao.dao.ScoreDao.updateScore", score);
	}
	public int updateScoreM(Score score) {
		return this.writerSqlSession.update("com.aixuexiao.dao.ScoreDao.updateScoreM", score);
	}
	
	public int getScoreByUserid(int userid){
		try{
			return this.writerSqlSession.selectOne("com.aixuexiao.dao.ScoreDao.selectScoreByUserid", userid);
		}catch(Exception e){
			return 0;
		}
	}
	public int getLastScore(int userid){
	    try {
            int score = this.readSqlSession.selectOne("com.aixuexiao.dao.ScoreDao.selectLastScore",userid);
            return score>=5?5:score+1;
        }catch(Exception e) {
            return 1;
        }
	}
}
