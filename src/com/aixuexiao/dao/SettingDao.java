package com.aixuexiao.dao;

import org.springframework.stereotype.Component;

import com.aixuexiao.model.Setting;

@Component("settingDao")
public class SettingDao extends BaseDao {


	public Setting isAdministrator(String name) {
		return this.readSqlSession.selectOne("com.aixuexiao.dao.SettingDao.selectAministrator",name);
	}
	public int updateAdministrator(Setting setting) {
		return this.readSqlSession.update("com.aixuexiao.dao.SettingDao.updateAdministrator",setting);
	}
	
}
