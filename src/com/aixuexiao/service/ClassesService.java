package com.aixuexiao.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aixuexiao.dao.ClassesDao;
import com.aixuexiao.dao.ClassesNewsDao;
import com.aixuexiao.dao.StudentDao;
import com.aixuexiao.model.Classes;
import com.aixuexiao.model.ClassesNews;
import com.aixuexiao.model.Student;

@Service("classesService")
public class ClassesService {

	@Resource(name="studentDao")
	private StudentDao studentDao;
	
	@Resource(name="classesDao")
	private ClassesDao classesDao;
	
	@Resource(name="classesNewsDao")
	private ClassesNewsDao classesNewsDao;
	
	/**
	 * 将数据库中Classes数据分页查出
	 * @param start 其实数据条数
	 * @param size  展示数据每页的大小
	 */
	public List<Classes> listClasses(int start,int size,Classes classes){
		return classesDao.findClasses(start, size,classes);
	}
	public int countClasses(Classes classes){
		return classesDao.countClasses(classes);
	}
	public void deleteClassesNewsById(int id){
		classesNewsDao.deleteClassesNewsById(id);
	}
	
	/**
	 * 添加班级动态到数据库
	 * @param classesNews
	 */
	public void addClassesNews(ClassesNews classesNews){
		classesNewsDao.addClassesNews(classesNews);
	}
	
	/**
	 * 获取指定班级的班级动态(钱1000个)
	 * @param classid 班级id
	 * @return
	 */
	public List<ClassesNews> findClassesNewsByClassId(int classid){
		return classesNewsDao.findClassesNewsByClassId(classid, 1000);
	}
	
	/**
	 * 添加班级到数据库中
	 * @param classes 班级对象
	 */
	public void addClasses(Classes classes){
		classesDao.addClasses(classes);
	}
	/**   
	 * @Title: updateClasses   
	 * @Description: 修改公司信息
	 * @param classes
	 * @author  author
	 */
	 
	public void updateClasses(Classes classes) {
		classesDao.updateClasses(classes);
	}
	/**
	 * 删除数据库中对应id的公司信息
	 * @param classesId 公司id
	 */
	public void deleteclasses(int classesId){
		classesDao.deleteclasses(classesId);
	}
	/**
	 * 根据id查找对应的Classes对象
	 * @param id 班级编号
	 * @return
	 */
	public Classes findClassesById(int id){
		return classesDao.findClassesById(id);
	}
	
	/**
	 * 根据员工编号查找对应的员工
	 * @param studentid 员工编号
	 * @return 员工数据
	 */
	public Student findStudentById(int studentid){
		return studentDao.findStudentById(studentid);
	}
	
	/**
	 * 根据班级id查找对应班级所有员工
	 * @param classesid 班级id
	 * @return
	 */
	public List<Student> findStudentByClassesId(int classesid){
		return studentDao.findStudentByClassesId(classesid);
	}
	
	/**
	 * 添加员工信息至数据库中
	 * @param student 员工对象
	 */
	public void addStudent(Student student){
		studentDao.addStudent(student);
	}
	
	
	/**
	 * 删除数据库中对应id的员工信息
	 * @param student 员工对象
	 */
	public void deleteStudentById(int studentid){
		studentDao.deleteStudentById(studentid);
	}
	
	/**
	 * 自动更新指定班级的员工数量
	 * @param classid 班级id
	 */
	public void updateClassStudentCount(int classid){
		classesDao.updateClassStudentCount(classid);
	}
	
	public void updateStudentBy(Student student) {
		studentDao.updateStudent(student);
	}
	
}
