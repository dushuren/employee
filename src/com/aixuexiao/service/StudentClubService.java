package com.aixuexiao.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aixuexiao.dao.StudentClubDao;
import com.aixuexiao.model.StudentClub;

@Service("studentClubService")
public class StudentClubService {

	
	@Resource(name="studentClubDao")
	private StudentClubDao studentClubDao;
	
	/**
	 * 将数据库中StudentClub数据分页查出
	 * @param start 其实数据条数
	 * @param size  展示数据每页的大小
	 */
	public List<StudentClub> listStudent(int start,int size,StudentClub studentClub){
		return studentClubDao.selectStudentClub(start,size,studentClub);
	}
	public int count(StudentClub studentClub){
		return studentClubDao.count(studentClub);
	}
	/**
	 * 根据id删除申请
	 * @param id
	 */
	public void deleteStudentClubById(int id){
		studentClubDao.deleteStudentClubById(id);
	}
	/**
	 * 更细申请信息状态
	 * @param clubId 公司id
	 */
	public void studentclubchangestate(int clubId,int status){
		studentClubDao.studentclubchangestate(clubId,status);
	}
	public StudentClub findStudentClubCount(int stuid,int clubid){
		return studentClubDao.findStudentClubCount(stuid,clubid);
	}
}
