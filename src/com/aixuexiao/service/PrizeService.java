package com.aixuexiao.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aixuexiao.dao.PrizeDao;
import com.aixuexiao.model.Prize;

@Service("prizeService")
public class PrizeService {

	@Resource(name="prizeDao")
	private PrizeDao prizeDao;
	
	
	public List<Prize> listPrize(int start,int size,Prize prize){
		return prizeDao.findPrize(start,size,prize);
	}
	public int countPrize(Prize prize){
		return prizeDao.countPrize(prize);
	}
	public Prize findPrizeById(int prizeid){
		return prizeDao.findPrizeById(prizeid);
	}
	public int addPrize(Prize prize){
		return prizeDao.addPrize(prize);
	}
	public int deletePrizeById(int prizeid) {
		return prizeDao.deletePrizeById(prizeid);
	}
	public int updatePrize(Prize prize) {
		return prizeDao.updatePrize(prize);
	}
}
