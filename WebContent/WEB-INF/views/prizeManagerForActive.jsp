<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>奖品管理</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@include file="/WEB-INF/views/common/css.jsp" %>
	</head>
	<body>
		<%@ include file="/WEB-INF/views/common/navbar.jsp" %>
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
			<%@ include file="/WEB-INF/views/common/sidebar.jsp" %>
			<div class="main-content">
				 
				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							<small>
								<i class="icon-list-alt"></i>
								奖品管理-奖品信息只能显示前4个
							</small>
						</h1>
					</div> 
					<div class="row-fluid">
						<div class="span12">
							<button class="btn btn-info" id="leavemessage" type="button" onclick="showAddModel()">
								<i class="icon-inbox"></i>
								添加
							</button>
							<button class="btn btn-info" type="button" onclick="location.href='<%=request.getContextPath() %>/manager/prizeActive'" >
								<i class="icon-arrow-left"></i>
								返回
							</button>
							<br/>
							<table id="sample-table-1" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th width="10%">编号</th>
										<th width="20%">奖品名称</th>
										<th width="20%">奖品</th>
										<th width="10%">奖品数量</th>
										<th width="15%">更新时间</th>
										<th width="40%" >操作</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${prizeList}"  var="prize"  >
									<tr>
										<td>${prize.id}</td>
										<td>${prize.name}</td>
										<td><img src="../${prize.img }" width="80px" height="80px"/></td>
										<td>${prize.num}</td>
										<td><fmt:formatDate value="${prize.optiondate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td >
											<button class="btn btn-mini btn-primary" onclick="if(window.confirm('确认要删除奖品${prize.name}？')==true)location.href='<%=request.getContextPath() %>/manager/deleteactive?id=${prize.id}'" ><i class="icon-remove"></i>&nbsp;删除</button>
											<a href="#myModal"  role="button" onclick="setvalue('${prize.id}','${prize.name}','${prize.img }','${prize.num }')" class="btn  btn-mini btn-info" data-toggle="modal"><i class="icon-edit"></i>编辑</a>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
					 		<!-- Modal -->
							<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  	<form action="<%=request.getContextPath() %>/manager/updateprize" id="updateactive" class="form-inline"  method="post" enctype="multipart/form-data">
								  <div class="modal-header">
								    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								    <h3 id="myModalLabel">编辑奖品</h3>
								  </div>
								  <div class="modal-body">
								  		<input type="hidden"  name="id"  id="editid" >
								  		<input type="hidden"  name="prizeactiveid"  id="prizeactiveid" value="${activeid }"/>
										<label class="control-label"   >奖品名称:&nbsp;&nbsp;</label>
										<input type="text" name="name" class="input-medium"  id="editname"  >
										<br><br>
										<label class="control-label"    >奖品图片:&nbsp;&nbsp;</label>
										<input type="file" name="file" id="file" class="input-medium" accept="image/gif, image/x-ms-bmp, image/bmp,image/jpeg,image/x-png" >
										<br><br><label class="control-label"   >奖品数量:&nbsp;&nbsp;</label>
										<input type="text" name="num" class="input-medium"  id="editnum"  >
								  </div>
								  <div class="modal-footer">
								    <button  type="button" id="modify" class="btn btn-small btn-primary">更新</button>
								  </div>
							  	</form>
							</div>
							<!-- Modal -->
							<div id="myaddModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  	<form action="<%=request.getContextPath() %>/manager/addprize" id="addactive" method="post"  class="form-inline" enctype="multipart/form-data">
								  <div class="modal-header">
								    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								    <h3 id="myModalLabel">添加奖品</h3>
								  </div>
								  <div class="modal-body">
								  		<input type="hidden"  name="prizeactiveid"  id="prizeactiveid" value="${activeid }"/>
										<label class="control-label"   >奖品名称:&nbsp;&nbsp;</label>
										<input type="text" name="name" id="addname" class="input-medium"    >
										<br><br>
										<label class="control-label"    >奖品图片:&nbsp;&nbsp;</label>
										<input type="file" name="file" id="file" class="input-medium"  accept="image/gif, image/x-ms-bmp, image/bmp,image/jpeg,image/x-png" >
										<br><br><label class="control-label"   >奖品数量:&nbsp;&nbsp;</label>
										<input type="text" name="num" class="input-medium"  id="addnum"  >
								  </div>
								  <div class="modal-footer">
								    <button  type="button" id="modify1" class="btn btn-small btn-primary">添加</button>
								  </div>
							  	</form>
							</div>
							<!--PAGE CONTENT ENDS-->
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div><!--/.page-content-->
		</div><!--/.main-content-->
	</div><!--/.main-container-->
	<%@include file="/WEB-INF/views/common/js.jsp" %>
	<script type="text/javascript">
		$(function() {
			$('#modify').on('click', function() {
				if($.trim($("#editname").val())==''){
					alert('请输入奖品名称！');
					return;
				}else if($.trim($("#editnum").val())==''){
					alert('请输入奖品数量！');
					return;
				}else if(isNaN(parseInt($("#editnum").val()))){
					alert('奖品数量必须为整数！');
					return;
				}else{
					$("#updateactive").submit();
				}
			});
			$('#modify1').on('click', function() {
				if($.trim($("#addname").val())==''){
					alert('请输入奖品名称！');
					return;
				}else if($.trim($("#addnum").val())==''){
					alert('请输入奖品数量！');
					return;
				}else if(isNaN(parseInt($("#addnum").val()))){
					alert('奖品数量必须为整数！');
					return;
				}else{
					$("#addactive").submit();
				}
			});
		});
		function setvalue(id,name,img,num){
			$("#editid").val(id);
			$("#editname").val(name);
			$("#editnum").val(num);
		}
		function showAddModel(){
			$('#myaddModal').modal();
		}
	</script>
	</body>
</html>