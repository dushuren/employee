<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
		<title>员工登陆</title>
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<!--basic styles-->
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-fonts.css" />
		<!--ace styles-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-ie.min.css" />
		<![endif]-->
	</head>
	<body class="login-layout">
			<div class="main-container container-fluid">
				<div class="main-content">
							<div class="login-container">
								<div class="row-fluid">
									<div class="position-relative">
										<div id="login-box" class="login-box visible widget-box no-border">
											<div class="widget-body">
												<div class="widget-main">
													<br/>
													<div class="space-20"></div>
													<!-- logo -->
													<div class="row-fluid">
														<div class="center">
															<h1>
																<span class="red"><img alt="" src="assets/images/LOGO.png"/></span>
																<span class="white"></span>
															</h1>
														</div>
													</div>
													<div class="space-20"></div>
	
													<form action="<%=request.getContextPath()%>/jiebang"  id="login" >
														
														<fieldset>
															<label class="center" style="background-color: #FFFFFF;border-top:1px solid #dadada;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																	<small>
																		&nbsp;${message } 
																	</small>
																	<input type="hidden" value="${student.id }" name="id"/>
															</label>
															<div class="space"></div>
																			
															<div class="row-fluid">
																<div class="center">
																	<h1 style="padding-left: 26px;padding-right: 26px;">
																		<span class="red" style="cursor: pointer;" onclick="checkValidate()"><img alt="" src="assets/images/btn_quxiao.png"/></span>
																	</h1>
																</div>
															</div>
															
															<div class="space-4">
															</div>
														</fieldset>
													</form>
													
												</div><!--/widget-main-->
											</div><!--/widget-body-->
										</div><!--/login-box-->
	
									</div><!--/position-relative-->
								</div>
							</div>
				</div>
			</div><!--/.main-container-->
			<!--basic scripts-->
	
			<!--[if !IE]>-->
	
			<script type="text/javascript">
				window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
			</script>
			<!--<![endif]-->
	
			<!--[if IE]>
			<script type="text/javascript">
			 window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
			</script>
			<![endif]-->
	
			<script type="text/javascript">
				if("ontouchend" in document) document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
			</script>
			<script src="<%=request.getContextPath()%>/assets/js/bootstrap.min.js"></script>
	
			<!--page specific plugin scripts-->
	
			<!--ace scripts-->
	
			<script src="<%=request.getContextPath()%>/assets/js/ace-elements.min.js"></script>
			<script src="<%=request.getContextPath()%>/assets/js/ace.min.js"></script>
			<script type="text/javascript">
				function checkValidate(){
					if(window.confirm('确认要解绑吗？')){
						document.getElementById('login').submit();
					}
				}
			</script>
		</body>
</html>