<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
		<title>员工留言</title>
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<!--basic styles-->
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-fonts.css" />
		<!--ace styles-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-ie.min.css" />
		<![endif]-->
		<style type="text/css">
			span
            {
                line-height:2.5;
            }
		</style>
	</head>
	<body class="login-layout">
			<div class="main-container container-fluid">
				<div class="main-content">
							<div class="login-container">
								<div class="row-fluid">
									<div class="position-relative">
										<div id="login-box" class="login-box visible widget-box no-border">
											<div class="widget-body">
												<div class="widget-main">
													<div class="row-fluid">
														<div class="center">
															<h1 style="margin: 1px 0;">
																<span class="red"><img alt="" src="assets/images/btn_liuyan.jpg"/></span>
																<span class="white"></span>
															</h1>
														</div>
													</div>
													<label class="center"> 
														<b>请填写以下信息</b>
													</label>
	
													<form action="<%=request.getContextPath()%>/addWeixinmessage" id="addmessage" method="post" >
														
														<fieldset>
															<label style="background-color: #FFFFFF;border-top:1px solid #dadada;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;姓名&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	${student.name }
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-remove-circle"></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;公司&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	${student.className }
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-chevron-down"></i>
																</span>
															</label>
															<label  style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right ">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;工号&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	${student.id }
																	<input type="hidden"  id="studentid" name="studentid" value="${student.id }" />
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-remove-circle" ></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;短号&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	${student.phone }-${student.remark}
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-remove-circle" ></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:0px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right ">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;留言&nbsp;&nbsp;&nbsp;&nbsp;</b>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block text-center">
																	<textarea style="width:95%" name="content" id="content" rows="4" placeholder="留言内容"></textarea>
																</span>
															</label>
															<div class="row-fluid">
																<div class="center">
																	<h1 style="padding-left: 26px;padding-right: 26px;">
																		<span class="red" style="cursor: pointer;" onclick="checkValidate()"><img alt="" src="assets/images/btn_submit.png"/></span>
																	</h1>
																</div>
															</div>
															<div class="center">
															${notice }
															</div>
															<div class="space-4">
															</div>
														</fieldset>
													</form>
													
												</div><!--/widget-main-->
											</div><!--/widget-body-->
										</div><!--/login-box-->
	
									</div><!--/position-relative-->
								</div>
							</div>
				</div>
			</div><!--/.main-container-->
			<!--basic scripts-->
	
			<!--[if !IE]>-->
	
			<script type="text/javascript">
				window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
			</script>
			<!--<![endif]-->
	
			<!--[if IE]>
			<script type="text/javascript">
			 window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
			</script>
			<![endif]-->
	
			<script type="text/javascript">
				if("ontouchend" in document) document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
			</script>
			<script src="<%=request.getContextPath()%>/assets/js/bootstrap.min.js"></script>
	
			<!--page specific plugin scripts-->
	
			<!--ace scripts-->
	
			<script src="<%=request.getContextPath()%>/assets/js/ace-elements.min.js"></script>
			<script src="<%=request.getContextPath()%>/assets/js/ace.min.js"></script>
			<script type="text/javascript">
				function checkValidate(){
					if(!$("#content").val()){
						return alert("请输入留言信息！");
						return false;
					}
					document.getElementById('addmessage').submit();
				}
			</script>
		</body>
</html>