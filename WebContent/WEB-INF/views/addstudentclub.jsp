<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>管理成员</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@include file="/WEB-INF/views/common/css.jsp" %>
	</head>

	<body>
		<%@ include file="/WEB-INF/views/common/navbar.jsp" %>
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
			<%@ include file="/WEB-INF/views/common/sidebar.jsp" %>
			<div class="main-content">
				 
				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							<small>
								<i class="icon-list-alt"></i>
								管理成员
							</small>
						</h1>
					</div> 
					<div class="row-fluid">
						
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<label for="form-field-8"> 俱乐部: <b>${club.name}</b> （${club.des }） </label>
							<br>
							<form id="searchStudentClubForm" class="form-inline" method="get" action="<%=request.getContextPath() %>/manager/studentclubmanagerpage" >
									<input type="hidden" id="pagenum" name="pagenum" value="1"/>
									<input type="hidden" id="clubid" name="clubid" value="${club.id}" />
									<label class="control-label"   >成员编号:&nbsp;&nbsp;</label>
									<input type="text" name="studentId" value="${studentId }"  class="input-small search-query"/>
									&nbsp;&nbsp;
									<label class="control-label"   >成员姓名:&nbsp;&nbsp;</label>
									<input type="text" name="studentname" id="studentname"  placeholder="姓名" value="${studentClub.studentname }" class="input-small search-query">
									&nbsp;&nbsp;
									<c:if test="${type!=2 }">
									<label class="control-label"    >状态:&nbsp;&nbsp;</label>
									<select name="status" class="input-medium">
										<option value="0">请选择</option>
										<option value="2" <c:if test="${studentClub.status == 2}">selected="selected"</c:if>>审核通过</option>
										<option value="1" <c:if test="${studentClub.status == 1}">selected="selected"</c:if>>待审核</option>
									</select>&nbsp;&nbsp;
									</c:if>
									<c:if test="${type==2 }">
									<input type="hidden" name="status" value="${type}" />
									</c:if>
									<button class="btn btn-small btn-info" id="addstudentclub" type="submit">
										<i class="icon-search"></i>
										查询
									</button>
									<button class="btn btn-small btn-info"  onclick="location.href='<%=request.getContextPath() %>/manager/club'" type="button">
										<i class="icon-arrow-left"></i>
										返回
									</button>
							</form>
							<c:if test="${param.notice != null}">
								<div class="alert alert-info">
									<button type="button" class="close" data-dismiss="alert">
										<i class="icon-remove"></i>
									</button>
									<i class="icon-ok"></i>
									<strong>${param.notice}</strong>
								</div>
							</c:if>
							<hr>
							<table id="sample-table-1" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th width="5%">#</th>
										<th width="10%">成员编号</th>
										<th width="15%">成员姓名</th>
										<th width="15%">申请时间</th>
										<th>留言</th>
										<th width="15%">操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${grid.rows}"  var="student" varStatus="sta" >
									<tr>
										<td>${student.id}</td>
										<td>${student.studentid}</td>
										<td>${student.studentname}</td>
										<td><fmt:formatDate value="${student.inserttime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td>${student.content}</td>
										<td>
											<c:if test="${student.status!=2 }">
												<button class="btn btn-mini btn-primary" onclick="location.href='<%=request.getContextPath() %>/manager/studentclubchangestate?id=${student.id}&clubid=${student.clubid }&status=2'" ><i class=" icon-ok"></i>&nbsp;通过</button>
											</c:if>
											<button class="btn btn-mini btn-danger" onclick="if(window.confirm('确认删除成员${student.studentname}申请？')==true)location.href='<%=request.getContextPath() %>/manager/deletestudentclub?id=${student.id}&clubid=${student.clubid }'"><i class="icon-remove"></i>移除</button>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
							<div class="dataTables_paginate paging_bootstrap pagination">
							  <c:if test="${grid.pages > 1}">
							  <button class="btn btn-success btn-mini" onclick="searchStudentClub(1)">首页</button>
							  <select name="classid" id="editclassid" class="btn btn-success input-medium" onchange="searchStudentClub(this.value)">
							  	<c:forEach  varStatus="num" begin="1" end="${grid.pages }">
									<option value="${num.index }" <c:if test="${num.index ==grid.currentPage }"> selected="selected"</c:if>>第${num.index }页</option>
								</c:forEach>
							  </select>
							  <button class="btn btn-success btn-mini" onclick="searchStudentClub(${grid.pages })" >末页</button>
							 </c:if>
					 		</div>
							<!--PAGE CONTENT ENDS-->
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div><!--/.page-content-->
		</div><!--/.main-content-->
	</div><!--/.main-container-->

		<%@include file="/WEB-INF/views/common/js.jsp" %>
		<script type="text/javascript">
		function searchStudentClub(pagenum){
			$("#pagenum").val(pagenum);
			$("#searchStudentClubForm").submit();
		}
		</script>
	</body>
</html>