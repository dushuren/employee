====2016-03-10====
1、处理查询乱码问题
tomacat对GET和POST请求处理方式是不同的，要处理针对GET请求的编码问题，则需要改tomcat的server.xml配置文件，如下：
<Connector connectionTimeout="20000" port="8080" protocol="HTTP/1.1" redirectPort="8443"/>
改为：
<Connector connectionTimeout="20000" port="8080" protocol="HTTP/1.1" redirectPort="8443" URIEncoding="UTF-8"/>
=========

一个基于SAE Java平台的微信平台公众帐号应用例子。

微信公众帐号：甬员工之家


微信公众帐号功能为作为学生家长和学校老师之间的沟通桥梁。
提供使用微信进行员工注册、登陆，进行公众号集成，抽奖等

应用基于SAE Java平台开发，使用（Spring/SpringMVC/Mybatis）框架开发，有相同需求（在SAE Java平台搭建微信公众帐号后台）的同学可用拿去作为参考，当然也可用作为在SAE Java平台中使用框架的例子程序作为参考。

微信的验证和回复的入口程序在com.aixuexiao.web.controller.WeixinController.java中。
其中initWeixinURL（对应URL:/weixin GET）方法为验证方法。
replyMessage（对应URL:/weixin POST）方法则是回复方法。

直接下载本项目在SAE Java平台上搭建需要改动地方：
1.在你创建SAE应用的MySQL中执行应用根目录下的app_aixuexiao.sql文件（包括表结构和部分测试数据）；
2.修改src下db.properties文件中的数据库信息ak、sk分别设置为你SAE应用中的ak和sk即可。


注：时间仓促且第一次使用SpringMVC，不优雅的代码敬请指出。

有任何问题可用联系我，看到第一时间回复

